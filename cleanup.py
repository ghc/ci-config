#!/usr/bin/env python3

"""
This script deletes build working directories older than the given maximum age.
"""

import shutil
from pathlib import Path
from datetime import datetime, timedelta
from time import time
from subprocess import run
import os
import platform
import stat
import logging
from typing import Iterator

if os.name == 'win32':
    from logging.handlers import NTEventLogHandler
    #logging.basicConfig(handlers=[NTEventLogHandler('gitlab-cleanup')])

MAX_AGE = timedelta(hours=8)

# This directory should contain the gitlab-runner `builds` and `cache`
# directories.
GITLAB_DIRS = {
    'Windows': Path(r'C:\GitLabRunner'),
    'Darwin': Path(r'/Users/builder')
}

GITLAB_DIR = GITLAB_DIRS[platform.system()]

logging.basicConfig(level=logging.INFO)

def robust_rm(path: Path):
    # First move the directory out of the way to narrow a race window
    # where gitlab-runner sees and attempts to use the directory
    # while it is being deleted. We could do even better by waiting
long enough for a copy to occur before initiating deletion.
    tmp = path.with_suffix('.tmp')
    shutil.move(path, tmp)

    def on_error(fn, path2, excinfo):
        os.chmod(path2, stat.S_IWRITE)
        os.remove(path2)

    shutil.rmtree(tmp, onerror=on_error)

def iter_if_dir(path: Path) -> Iterator[Path]:
    if path.is_dir():
        yield from path.iterdir()

def clean(max_age: timedelta, dry_run: bool):
    # Clean cache
    for d in iter_if_dir(GITLAB_DIR / 'cache'):
        try:
            logging.info(f'deleting {d}')
            robust_rm(d)
        except Exception as e:
            logging.error(f'Error while deleting {d}: {e}')

    # Clean builds
    for runner_dir in iter_if_dir(GITLAB_DIR / 'builds'):
        for inst_dir in iter_if_dir(runner_dir):
            for user_dir in iter_if_dir(inst_dir):
                try:
                    ghc_dir = user_dir / 'ghc'
                    s = ghc_dir.stat()
                    mtime = datetime.fromtimestamp(s.st_mtime)
                    age = datetime.now() - mtime
                    delete = age > max_age

                    action = 'deleting' if delete else 'keeping '
                    logging.info(f'{action} {user_dir} (age={age})')
                    if delete and not dry_run:
                        robust_rm(user_dir)
                except Exception as e:
                    logging.error(f'Error while deleting {user_dir}: {e}')

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--dry-run', action='store_true')
    parser.add_argument('--max-age', default=timedelta(hours=8))
    args = parser.parse_args()

    clean(max_age=args.max_age, dry_run=args.dry_run)

if __name__ == '__main__':
    main()

