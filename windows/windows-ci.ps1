$msys64_url = "http://repo.msys2.org/distrib/x86_64/msys2-base-x86_64-20210228.tar.xz";
$git_url = "https://github.com/git-for-windows/git/releases/download/v2.25.0.windows.1/Git-2.25.0-64-bit.exe";

$ErrorActionPreference = "Stop"
# Cargo culting some junk from https://stackoverflow.com/questions/9948517/how-to-stop-a-powershell-script-on-the-first-error
$PSDefaultParameterValues['*:ErrorAction']='Stop'

function Throw-On-Native-Failure {
  if (-not $?) {
    throw 'Command failed'
  }
}

# Native git is significantly more efficient than Git from msys; use it
# instead.
function Install-Git {
  Invoke-WebRequest -Uri $git_url -OutFile git-setup.exe
  .\git-setup.exe /verysilent /suppressmsgboxes /norestart /nocancel /sp- /log
}

function Execute-Mingw {
  param( [string] $message
       , [string] $command
       , [bool] $ignoreExitCode = $false
       )
  # Set the APPDATA path which does not get inherited during these invokes
  # and set MSYSTEM to make sure we're using the right system
  $envdata = "export APPDATA=""" + $Env:AppData + """" +`
              " && export MSYSTEM=MINGW64" + `
              " && export PATH=/mingw64/bin:`$PATH" + `
              " && export CFLAGS=""-DONLY_CHOCOLATEY=0""" + `
              " && export CFLAGS=""-DUSE_BACKUPS=0""" + `
              " && "

  $msys = "c:\msys64";

  Write-Host "$message with '$command'..."
  $proc = Start-Process -NoNewWindow -UseNewEnvironment -Wait `
    $msys\usr\bin\bash.exe `
    -ArgumentList '--login', '-c', "'$envdata $command'" -PassThru

  if ((-not $ignoreExitCode) -and ($proc.ExitCode -ne 0)) {
    throw ("Command `'$command`' did not complete successfully. ExitCode: " + $proc.ExitCode)
  }
}

function Expand-Tarball($tarFile, $dest) {
  Write-Host "Expanding $tarFile"
  if (-not (Get-Command Expand-7Zip -ErrorAction Ignore)) {
    Install-Package -Scope CurrentUser -Force 7Zip4PowerShell > $null
  }

  $inner = Get-7Zip $tarFile
  Expand-7Zip $tarFile .
  Expand-7Zip tmp.tar $dest
  Remove-Item tmp.tar
}

function Fetch-Expand-Tarball($url, $dest) {
  Invoke-WebRequest -Uri $url -OutFile tmp.tar.xz
  Expand-Tarball tmp.tar.xz $dest
}

function Install-Msys() {
  if (-not (Test-Path C:\msys64 -PathType container)) {
    Write-Host "Installing msys64..."
    Fetch-Expand-Tarball $msys64_url C:\
  }
}

function Mingw-Deps {
  [OutputType([String])]
  Param( [string] $arch )

  "vim winpty git tar bsdtar unzip binutils autoconf make xz \
    curl libtool automake p7zip patch ca-certificates \
    python3 python3-pip \
    mingw-w64-$arch-gcc mingw-w64-$arch-python3-sphinx \
    mingw-w64-$arch-tools-git"
}

function Update-Msys() {
  # Approach taken from
  # https://github.com/Mistuke/GhcDevelChoco/blob/master/templates/chocolateyInstall-Template.ps1#L47
  Execute-Mingw "Update package DB" "pacman -Syy"
  Execute-Mingw "Update system packages" "pacman --noconfirm --needed -Sy bash pacman pacman-mirrors msys2-runtime"
  Execute-Mingw "Full system update" "pacman --noconfirm -Su"
  Execute-Mingw "Upgrading full system twice" "pacman --noconfirm -Su"
}

function Install-Deps() {
  Write-Host "Installing msys packages..."

  Update-Msys
  $deps = Mingw-Deps "x86_64"
  Execute-Mingw "Installing deps" "pacman --noconfirm --needed -Suy $deps"

  Write-Host "Installing Python dependencies..."
  Execute-Mingw "Installing deps" "pip install requests"
}

$ConfigData = @{
  AllNodes = @(
    @{
      NodeName = "localhost"
      PSDscAllowPlainTextPassword = $true
    }
  )
}

Install-Module -Name cUserRightsAssignment

$password = "902hjb9--@8hn"
$sec_password = ConvertTo-SecureString $password -AsPlainText -Force
$cred = New-Object System.Management.Automation.PSCredential ("gitlab-runner", $sec_password)

Configuration GitLabUser {
  Import-DscResource -ModuleName cUserRightsAssignment
  Import-DscResource -ModuleName PSDesiredStateConfiguration

  Node localhost {
    Registry EnableLongPaths {
      Ensure = "Present"
      Key = "HKLM:\SYSTEM\CurrentControlSet\Control\FileSystem"
      ValueName = "LongPathsEnabled"
      ValueData = "1"
      ValueType = "Dword"
    }

    User CreateUser {
      UserName = $cred.UserName
      Description = "GitLab Runner"
      Password = $cred
      PasswordNeverExpires = $true
      Ensure = "Present"
    }

    cUserRight GrantServiceRight {
      Ensure = "Present"
      Constant = "SeServiceLogonRight"
      Principal = $cred.UserName
      DependsOn = "[User]CreateUser"
    }

    cUserRight GrantSymlinkRight {
      Ensure = "Present"
      Constant = "SeCreateSymbolicLinkPrivilege"
      Principal = $cred.UserName
      DependsOn = "[User]CreateUser"
    }
  }
}

function Download-GitLab-Runner() {
  Write-Host "Downloading gitlab-runner..."
  mkdir -Force \GitLabRunner
  wget https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-amd64.exe -OutFile \GitLabRunner\gitlab-runner.exe
}

function Configure-GitLab-Runner-User() {
  Write-Host "Creating gitlab-runner user..."
  $OutputPath = Join-Path -Path ([System.IO.Path]::GetTempPath()) -ChildPath "GitLabUser"
  GitLabUser -OutputPath $OutputPath -ConfigurationData $ConfigData
  Start-DscConfiguration -Path $OutputPath -Force -Verbose -Wait

}

function Activate-GitLab-Runner() {
  Write-Host "Activating gitlab-runner..."
  mkdir -Force \GitLabRunner
  Push-Location \GitLabRunner
  # If it doesn't exist, download and install.
  # If it already exists, assume already installed and running. Stop and upgrade.
  # Finally, start
  If (-not (Test-Path \GitLabRunner\gitlab-runner.exe -type Leaf)) {
    Download-GitLab-Runner
    $username = $cred.UserName
    .\gitlab-runner.exe install --user ".\$username" --password "$password"
    Throw-On-Native-Failure
  }
  Else {
    Write-Host "...runner already exists. Stopping (allowed to fail)..."
    .\gitlab-runner.exe stop
    Download-GitLab-Runner
  }
  .\gitlab-runner.exe start
  Throw-On-Native-Failure
  Pop-Location
}

function Upgrade-GitLab-Runner() {
  Stop-Service gitlab-runner
  Download-GitLab-Runner
  Start-Service gitlab-runner
  \GitLabRunner\gitlab-runner.exe --version
}

function Configure-Filesystem-Opts() {
  Write-Host "Configuring filesystem options..."
  fsutil 8dot3name set 1
  fsutil behavior set disablelastaccess 1

  # See https://docs.gitlab.com/runner/faq/README.html#b-use-ntfssecurity-tools-for-powershell
  Install-Module -Name NTFSSecurity
}

function Add-Windows-Defender-Exclusions() {
  Write-Host "Adding Windows Defender exclusions..."
  Add-MpPreference -ExclusionPath @( "C:\GitLabRunner", "C:\msys32", "C:\msys64" )
  Add-MpPreference -ExclusionProcess @( "ghc.exe", "gcc.exe", "ld.exe", "collect2.exe", "git.exe" )

  # The big hammer...
  # We have found that even with the above Defender still chews through
  # half a CPU.
  Set-MpPreference -DisableRealtimeMonitoring $true
}

# FIXME: not idempotent. Need to check if the job already exists or something.
function Install-Cleanup-Task() {
  Write-Host "Installing cleanup task..."
  $trigger = New-JobTrigger -Once -At "0am" -RepetitionInterval (New-TimeSpan -Hours 2) -RepetitionDuration ([TimeSpan]::MaxValue)
  $options = New-ScheduledJobOption -StartIfOnBattery
  Register-ScheduledJob `
    -Name "GitLab Cleanup" `
    -ScriptBlock {
      Get-ChildItem -Path C:\GitLabRunner\builds\0\ -Directory | where {$_.LastWriteTime -le $(Get-Date).AddHours(-6)} | Remove-Item -Recurse -Force
      Get-ChildItem -Path C:\GitLabRunner\builds\1\ -Directory | where {$_.LastWriteTime -le $(Get-Date).AddHours(-6)} | Remove-Item -Recurse -Force
    } `
    -Trigger $trigger `
    -ScheduledJobOption $options
}

function Register-GitLab-Runner {
  Param( [Parameter(Mandatory=$true)] [string] $regToken )
  Param( [Parameter(Mandatory=$true)] [int] $cpus )

  Push-Location \GitLabRunner
  $url = "https://gitlab.haskell.org/"
  C:\GitLabRunner\gitlab-runner.exe register `
    --non-interactive `
    --url $url --registration-token $regToken `
    --output-limit 50000000 `
    --env "PATH=C:\Program Files\Git\cmd;C:\msys64\usr\bin" `
    --env CPUS=$cpus `
    --env MSYS64=c:\msys64 `
    --env MSYS32=c:\msys32 `
    --env GIT_DEPTH=10 `
    --env 'GIT_CLONE_PATH=$CI_BUILDS_DIR/$CI_CONCURRENT_ID/$CI_JOB_ID' `
    --executor shell --shell powershell `
    --tag-list new-x86_64-windows,test `
    --custom_build_dir-enabled `
    --request-concurrency 2
  Pop-Location
}

function Fixup-Git() {
  $env:PATH="C:\\Program Files\\Git\\cmd;C:\\msys64\\usr\\bin;C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Windows\System32\OpenSSH\;C:\Program Files\WireGuard\;C:\Program Files\Git\cmd;C:\Users\Administrator\AppData\Local\Microsoft\WindowsApps;"
  git config --system core.eol lf
  git config --system core.autocrlf false
}

Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip {
    param([string]$zipfile, [string]$outpath)

    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}

function Install-Wireguard-Tunnel() {
  Invoke-WebRequest -Uri https://download.wireguard.com/windows-client/wireguard-amd64-0.3.9.msi -OutFile wg.msi
  .\wg.msi
  Start-Process 'C:\Program Files\WireGuard\wireguard.exe' -ArgumentList '/installtunnelservice', 'C:\Users\Administrator\zw3rk-wg.conf' -Wait -NoNewWindow -PassThru | Out-Null
  Start-Process sc.exe -ArgumentList 'config', 'WireGuardTunnel$zw3rk-wg', 'start= delayed-auto' -Wait -NoNewWindow -PassThru
  Start-Service -Name 'WireGuardTunnel$zw3rk-wg'
  Start-Service -Name 'WireGuardTunnel$zw3rk-wg' -ErrorAction SilentlyContinue
}

function Uninstall-Cleanup-Task() {
  Unregister-ScheduledJob -Name "GitLab Cleanup"
}

function Install-Windows-Exporter() {
  Invoke-WebRequest -Uri https://github.com/prometheus-community/windows_exporter/releases/download/v0.16.0/windows_exporter-0.16.0-amd64.msi -OutFile windows-exporter.msi
  .\windows-exporter.msi
}

function Install-Nodepad-PlusPlus {
  Invoke-WebRequest -Uri https://github.com/notepad-plus-plus/notepad-plus-plus/releases/download/v7.9.5/npp.7.9.5.Installer.x64.exe -OutFile npp.exe
  .\npp.exe
}

function Install-Process-Monitor {
  $url = "https://download.sysinternals.com/files/ProcessMonitor.zip"
  Invoke-WebRequest -Uri $url -OutFile ProcessMonitor.zip
  Unzip ProcessMonitor.zip C:\Users\bgamari_ghc
}

# putting cleanup files, prevents windows from failing over if
# a file handle hasn't been released and we already try to delete
# the install file.
function Cleanup-Files() {
  Remove-Item git-setup.exe
  #Remove-Item npp.exe
  #Remote-Item windows-exporter.msi
}

# Install a GHC build environment.
function Configure-GHC-Env() {
  Install-Git
  Install-Msys
  Install-Deps
  Fixup-Git
  Configure-Filesystem-Opts
  Add-Windows-Defender-Exclusions
}

# Install a GHC build environment and configure gitlab-runner.
function Configure-GHC-Runner() {
  Configure-GHC-Env
  Configure-GitLab-Runner-User
  Activate-GitLab-Runner
  Install-Cleanup-Task
  Cleanup-Files
}

$CrashDumpDir = "C:\CrashDumps"

Configuration CrashDumps {
  Node localhost {
    Registry DumpFolder {
      Ensure = "Present"
      Key = "HKLM:\SOFTWARE\Microsoft\Windows\Windows Error Reporting\LocalDumps\DumpFolder"
      ValueName = ""
      ValueData = $CrashDumpDir
      ValueType = "ExpandString"
    }
    Registry DumpCount {
      Ensure = "Present"
      Key = "HKLM:\SOFTWARE\Microsoft\Windows\Windows Error Reporting\LocalDumps\DumpCount"
      ValueName = ""
      ValueData = "10"
      ValueType = "Dword"
    }
    Registry DumpType {
      Ensure = "Present"
      Key = "HKLM:\SOFTWARE\Microsoft\Windows\Windows Error Reporting\LocalDumps\DumpType"
      ValueName = ""
      ValueData = "2"
      ValueType = "Dword"
    }
    File DumpDir {
      DestinationPath = $CrashDumpDir
      Type = "Directory"
      Credential = $cred
    }
  }
}

function Enable-Crash-Dumps() {
  Write-Host "Creating gitlab-runner user..."
  $OutputPath = Join-Path -Path ([System.IO.Path]::GetTempPath()) -ChildPath "CrashDumps"
  CrashDumps -OutputPath $OutputPath -ConfigurationData $ConfigData
  Start-DscConfiguration -Path $OutputPath -Force -Verbose -Wait
}
