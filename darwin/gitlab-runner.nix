{ pkgs, ... }:

{
  services.gitlab-runner = {
    enable = true;
    extraPackages = [ pkgs.nix pkgs.git ];
    services = {
      "gitlab.haskell.org" = {
        authenticationTokenConfigFile = "/etc/gitlab-auth-token";
        registrationFlags = [
          "--output-limit 16384"
        ];
        executor = "shell";
        environmentVariables = {
          NIX_REMOTE = "daemon";
          PATH = "/var/run/current-system/sw/bin:/nix/var/nix/profiles/default/bin:/nix/var/nix/profiles/default/sbin:/bin:/sbin:/usr/bin:/usr/sbin";
          NIX_SSL_CERT_FILE = "/nix/var/nix/profiles/default/etc/ssl/certs/ca-bundle.crt";
        };
      };
    };
  };
}
