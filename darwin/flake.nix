{
  description = "GHC Darwin CI runner";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-24.11-darwin";
    nix-darwin.url = "github:bgamari/nix-darwin/wip/gitlab-runner-24.11";
    nix-darwin.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs@{ self, nix-darwin, nixpkgs }:
    let
      mkAArch64Config = mkConfig "aarch64-darwin";
      mkIntelConfig = mkConfig "x86_64-darwin";

      mkConfig = platform: hostName:
        nix-darwin.lib.darwinSystem {
          modules = [
            ./configuration.nix
            ./clean-rosetta.nix
            ./gitlab-runner.nix
            {
              networking.hostName = hostName;
              system.configurationRevision = self.rev or self.dirtyRev or null;
              nixpkgs.hostPlatform = platform;
            }
          ];
        };

    in
    {
      # Build darwin flake using:
      # $ darwin-rebuild build --flake .#<hostname>
      darwinConfigurations."Mini18-Alpha" = mkIntelConfig "Mini18-Alpha";
      darwinConfigurations."Mini18-Beta" = mkAArch64Config "Mini18-Beta";
      darwinConfigurations."Mini18-Delta" = mkAArch64Config "Mini18-Delta";
      darwinConfigurations."Mini18-Epsilon" = mkAArch64Config "Mini18-Epsilon";
      darwinConfigurations.bare =
        nix-darwin.lib.darwinSystem { modules = [ ./configuration.nix ]; };
      packages.aarch64-darwin.darwin-rebuild = nix-darwin.packages.aarch64-darwin.darwin-rebuild;
      packages.x86_64-darwin.darwin-rebuild = nix-darwin.packages.x86_64-darwin.darwin-rebuild;
    };
}
