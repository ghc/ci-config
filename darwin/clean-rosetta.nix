{ config, lib, pkgs, ... }:

{
  config.launchd.daemons.cleanup-rosetta-cache = {
    # XXX: ideally we'd figure out if c64..58 is the right path to kill.
    script = ''
      # graceful shutdown of gitlab-runner by sending SIGQUIT.
      pkill -SIGQUIT gitlab-runner
      # waiting for the process to be gone from launchctl.
      # launchctl will list the pid of the process first. '-' indicates
      # no pid (process terminated).
            while ! launchctl list | grep org.nixos.gitlab-runner | grep "^-"; do
        echo "waiting for gitlab-runner to exit..."
        sleep 10
      done
      launchctl stop org.nixos.gitlab-runner
      rm -fR /var/lib/gitlab-runner/builds
      rm -fR /var/lib/gitlab-runner/cache
      rm -fR /var/lib/gitlab-runner/arm64-home
      rm -fR /var/lib/gitlab-runner/x86_64-home
      rm -fR /System/Library/Caches/com.apple.coresymbolicationd
      # Big Sur
      rm -fR /private/var/db/oah/c642c66595f5050af26db8f86497ea83caff7cea5a9bcb2361ffa81d0e090b58
      # Monterey, and recent versions of Big Sur
      rm -fR /private/var/db/oah/279281326358528_279281326358528
      echo "restarting..."
      reboot
    '';
    serviceConfig = {
      # every day at 00:00h we'll stop gitlab-runner, drop the oah cache and reboot.
      StartCalendarInterval = [ { Hour = 0; Minute = 0; } ];
    };
  };
}
