{ config, pkgs, ... }:

{
  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages =
    [ pkgs.vim pkgs.mg pkgs.nano pkgs.tmux pkgs.git
    ];

  # Necessary for using flakes on this system.
  nix.settings.experimental-features = "nix-command flakes";

  # Auto upgrade nix package and the daemon service.
  services.nix-daemon.enable = true;

  # ensure we garbage collect once we hit 10G free,
  # collect, until at least 30G are free again.
  nix.gc.automatic = true;
  nix.extraOptions = ''
    extra-platforms = x86_64-darwin aarch64-darwin
    min-free = ${toString (10 * 1024 * 1024 * 1024)}
    max-free = ${toString (30 * 1024 * 1024 * 1024)}
    auto-optimise-store = true
  '';

  # need this to make copy-to work.
  nix.settings.trusted-users = [ "root" "haskell-ci" ];

  users.knownGroups = [ "gitlab-runner" ];
  users.knownUsers = [ "gitlab-runner" ];

  # Create /etc/bashrc that loads the nix-darwin environment.
  programs.zsh.enable = true;  # default shell on catalina

  # Used for backwards compatibility, please read the changelog before changing.
  # $ darwin-rebuild changelog
  system.stateVersion = 4;
}

