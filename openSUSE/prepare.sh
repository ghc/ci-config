#!/usr/bin/env bash

currentDir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
source ${currentDir}/base.sh

set -eo pipefail

# trap any error, and mark it as a system failure.
trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR

start_container() {
    if podman inspect "$CONTAINER_ID" >/dev/null 2>&1; then
        echo 'Found old container, deleting'
        podman kill "$CONTAINER_ID"
        podman rm "$CONTAINER_ID"
    fi

    mkdir -p "$CACHE_DIR"
    podman login -u $CUSTOM_ENV_CI_REGISTRY_USER -p $CUSTOM_ENV_CI_REGISTRY_PASSWORD $CUSTOM_ENV_CI_REGISTRY
    podman pull -q "$CUSTOM_ENV_CI_JOB_IMAGE"
    podman run \
        --detach \
        --interactive \
        --tty \
        --name "$CONTAINER_ID" \
        --volume "$CACHE_DIR":"/home/ghc/cache" \
        "$CUSTOM_ENV_CI_JOB_IMAGE"
}


echo "Running in $CONTAINER_ID"

start_container
