# GHC CI automation

This repository contains the automation for setup and maintenance of GHC's
continuous integration runners.

See the subdirectories for details of how the runners are configured.

## Adding a runner

You are welcome to ask for help at any stage of the process, including the very
beginning. Best would be to open a ticket on this repo.

Unfortunately the instructions are currently very terse, but we will improve
them over time.

Criteria for adding a runner:

1. Write a MR to this repository adding instructions for setting up the runner.
   Be as thorough and/or automated as possible. :) See the other configs below
   or in the subdirectories as examples.
2. Add the [maintainers' public ssh keys](./maintainer_keys.txt) to the runner
   system config.
    * Ensure they have administrator privileges, or can use sudo without a
      password.

Once the criteria are met, maintainers will provide an "instance token" that
makes the runner available to all repositories.

If you can't satisfy all the criteria, that's ok. Open an issue describing your
circumstance (and read on).

### Rationale for criteria

From the point of view of GHC contributors, CI is a service that needs to be
reliable. From a GHC *maintainer* perspective, it's even more critical: GHC
releases are built in CI, so platform support is dependent on CI runner
reliability.

1. Having documented and automated provisioning and configuration is a guarantee
   of future stability (although not an iron-clad one).
2. Having multiple maintainer means responsibility is shared, and critical
   issues get resolved faster.
    * In the future, having two tiers of access makes sense: admin-level access
      for CI maintainers, and unprivileged access for select GHC maintainers who
      need access to the platform for debugging. For now, Matt and Ben fill both roles.

### Generic instructions for GitLab runner configuration

*These are older notes, focused on Linux, but can be useful to accompany the
[official documentation][gitlab-docs]. Note that the output limit in particular
needs to be much higher than the default.*

[gitlab-docs]: https://docs.gitlab.com/runner/configuration/

You will need to determine the following things before registering your runner:

* `$name`: a friendly name of the runner; typically either a fully-qualified hostname or something like `gce-windows-3`
* `$jobs`: how many concurrent jobs the runner should run at once
* `$cores`: how many cores each of these jobs should take. `$cores * $jobs` should be less than or equal to the logical core count of the machine.
* `$token`: the current GitLab registration token. Note that these can be used only once. You can ask @bgamari for this.
* `$tags`: the set of [tags](#job-tags) which apply to the runner (probably `x86_64-linux`)

With this information in hand (and `gitlab-runner` installed) you can register your runner by running:
```
$ sudo gitlab-runner register \
    --description $name \
    --non-interactive \
    --registration-token $token \
    --output-limit 16000 \
    --url https://gitlab.haskell.org/ \
    --tag-list $tags \
    --env CPUS=$cores \
    --request-concurrency $jobs \
    --executor docker \
    --docker-image debian/stretch
```


Also add a cron job to prune things:
```bash
echo "@daily root docker system prune --all --force --volumes" | sudo tee /etc/cron.d/docker-prune
```

## Current runners

See the [Google Doc](https://docs.google.com/spreadsheets/d/1_UncQmtD5PkinLgq4DSB4Y5dy7PhOPPjPp6qnhZNA9w/edit#gid=0)

## Lesser supported (or incompletely supported) platforms

### Linux configuration (other than NixOS)

For instance, in the case of Debian Buster, first [install Docker](https://docs.docker.com/engine/install/debian/):
```bash
$ sudo apt-get update
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
$ curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
$ sudo apt-get update
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```
Next install GitLab runner:
```bash
$ curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
$ sudo apt-get install gitlab-runner
```
Then configure `gitlab-runner` as described in the generic instructions section above.

It is [necessary](https://gitlab.com/gitlab-org/gitlab-runner/issues/2980#note_131320536) to ensure that the Docker cache is periodically cleaned. Note that we do not use the cleanup script (`clear-docker-cache`) provided by GitLab as the `--filter` flag it passes means that it will not clean up images.

```bash
$ cat | sudo tee /etc/cron.daily/gitlab-clear-docker-cache <<EOF
#!/bin/sh -e
docker system prune --all --volumes --force
EOF
$ sudo chmod ug+rx /etc/cron.daily/gitlab-clear-docker-cache
```

### ARMv7 on AArch64 configuration

The `ghc-arm-5` runner supports running ARMv7 containers.

### PowerPC configuration

There are three PowerPC VMs hosted by the OSUOSL. One runs Fedora 29
and two run openSUSE Leap 15.4.

FIXME: The Fedora VM is not currently running gitlab-runner.

| Variable | name          | cores | suse_version |
| -------- | ------------- | ----- | ------------ |
| POWER8   | OSUOSL POWER8 | 8     | 15.4         |
| POWER9   | OSUOSL POWER9 | 8     | 15.5         |

#### POWER8 configuration
IP-address: 140.211.168.152

OS: openSUSE Leap 15.4

POWER8 machine runs gitlab-runner with custom executor driving podman.

Install gitlab-runner
```bash
sudo zypper ar -c -K -G -n gitlab
https://download.opensuse.org/repositories/home:/ptrommler:/branches:/home:/darix:/apps/$suse_version gitlab
sudo zypper ref
sudo zypper install gitlab-runner
```

Installing podman is done as follows:
```bash
sudo zypper in podman fuse3
sudo touch /etc/sub{u,g}id
sudo usermod --add-subuids 10000-75535 gitlab-runner
sudo usermod --add-subgids 10000-75535 gitlab-runner
rm -f /run/user/gitlab-runner/libpod/pause.pid
```

Install files `base.sh`, `prepare.sh`, `run.sh`, and `cleanup.sh` in
`/srv/gitlab-runner/ghc`.

The command to register differs from the generic case only in the
last five lines:

```
$ sudo gitlab-runner register \
    --description $name \
    --non-interactive \
    --registration-token $token \
    --output-limit 16000 \
    --url https://gitlab.haskell.org/ \
    --tag-list ppc64le-linux \
    --env CPUS=$cores \
    --request-concurrency 1 \
    --builds-dir /home/ghc \
    --cache-dir /home/ghc/cache \
    --custom-prepare-exec /srv/gitlab-runner/ghc/prepare.sh \
    --custom-run-exec /srv/gitlab-runner/ghc/run.sh \
    --custom-cleanup-exec /srv/gitlab-runner/ghc/cleanup.sh
```

```bash
$ cat | sudo tee /etc/cron.daily/podman-prune <<EOF
#!/bin/sh -e
podman system prune --all --volumes --force
EOF
$ sudo chmod a+x /etc/cron.daily/podman-prune
```

#### POWER9 configuration
IP-address: 140.211.168.176

OS: openSUSE Leap 15.5

POWER9 machine additionally runs gitlab-runner with docker executor and
podman as a replacement for docker.

Install as follows
```bash
sudo zypper install podman-docker fuse3
sudo touch /etc/sub{u,g}id
sudo usermod --add-subuids 10000-75535 gitlab-runner
sudo usermod --add-subgids 10000-75535 gitlab-runner
rm -f /run/user/gitlab-runner/libpod/pause.pid
```

And register gitlab-runner
```
$ sudo gitlab-runner register \
    --description $name \
    --non-interactive \
    --registration-token $token \
    --output-limit 16000 \
    --url https://gitlab.haskell.org/ \
    --tag-list ppc64le-linux \
    --env CPUS=$cores \
    --request-concurrency 1 \
    --executor docker \
    --docker-image openSUSE/leap15.4
```

To build docker images we also run a shell executor on POWER9. (TODO:
use buildah in a podman container, requires buildah image)


## Usage notes and maintainer tips

### How runners are used in GitLab

#### Job tags

The following job tags are currently defined:

 * `aarch64-linux`
 * `armv7-linux`
 * `ppc64le-linux`
 * `x86_64-linux`
 * `x86_64-linux-perf` (runners which can be relied on for stable performance numbers)
 * `x86_64-darwin`
 * `aarch64-darwin`
 * `new-x86_64-windows`
 * `head.hackage`: can run ghc/head.hackage> builds; this is limited to a subset of machines to increase cache reuse.
 * `trusted`: is run by a trusted entity and is therefore eligible to build release binary distributions
 * `lint`: a separate set of x86-64/Linux runner registrations to ensure that linters can always run with minimal latency
 * `nix` denotes a runner with `nix` installed.

#### Environment variables

The runners are configured to expose the following environment variables:

 * `CPUS`: The number of CPUs the job should use.

### Updating the bootstrap compiler

To update the GHC version used as the bootstrap compiler there are a few places that need to be updated:

 * for the Linux jobs update the `Dockerfile`s in the ghc/ci-images> project:
    * Update the binary distribution tarball URLs
    * Update the LLVM versions and tarball URLs (N.B. in the ARM and AArch64 case there are two relevant LLVM versions: the version needed by the bootstrap compiler and the version needed by GHC `master`).
 * for the Darwin and Windows jobs: the `GHC_TARBALL` and `CABAL_TARBALL` variables in `.gitlab-ci.yml` in the ghc/ghc> project need to be updated.

### Head.hackage builds

In addition to building GHC itself, we also provide CI jobs which build a subset of Hackage using the ghc/head.hackage> patchset. This builds can be triggered in one of three ways:

 * By manually running the CI job
 * By pushing to an MR bearing the ~user-facing label
 * Via the scheduled nightly builds

The `head.hackage` CI infrastructure is built on the Nix infrastructure in the `head.hackage` repository. The overall flow of control looks like this:

 1. The  ghc/ghc> pipeline's `hackage` job is triggered
 2. The `hackage` job triggers a pipeline of the ghc/head.hackage> project using a job trigger token owned by @head.hackage, passing its pipeline ID via the `GHC_PIPELINE_ID` variable
 3. The ghc/head.hackage> job uses the value of `GHC_PIPELINE_ID` to lookup the job ID of the pipeline's `x86_64-fedora27` job. This requires a personal access token which is provided by the @head.hackage user.
 4. The ghc/head.hackage> job fetches the binary distribution of the GHC `x86_64-fedora27` job
 5. The ghc/head.hackage> job uses [ghc-artefact-nix](https://github.com/mpickering/ghc-artefact-nix) to install the bindist
 6. The ghc/head.hackage> job uses `scripts/build-all.nix` to build a subset of Hackage (identified by the `testedPackages` attribute) using the bindist
 7. The ghc/head.hackage> job uses `scripts/summary.py` to produce a summary of the build results. This includes:
    1. a `dot` graph showing the package dependency graph and their failures
    2. a tarball containing the build log files
    3. log output showing abbreviated logs of failed builds

    (1) and (2) are then uploaded as artifacts.
