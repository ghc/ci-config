# Usage:
#
# {
#   inputs.ghc-gitlab-runner.url = "git+https://gitlab.haskell.org/ghc/ci-config";
#   outputs = inputs: {
#     nixosConfigurations.machine = nixpkgs.lib.nixosSystem {
#       modules = [
#         inputs.ghc-gitlab-runner.nixosModules.ghc-runner-common
#         inputs.ghc-gitlab-runner.nixosModules.ghc-runner
#         inputs.ghc-gitlab-runner.nixosModules.ghc-runner-freebsd
#       ];
#       {
#         ghc-gitlab-runner = {
#           name = "NAME";
#           cores = 16;
#         };
#       }
#     };
#   };
# }

{
  outputs = { self }: {
    nixosModules.ghc-runner-common = { lib, ... }: {
      config.virtualisation.docker = {
        enable = true;
        autoPrune = {
          enable = true;
          flags = [ "--force" "--all" "--volumes" ];
          dates = "daily";
        };
      };

      options.ghc-gitlab-runner.name = lib.mkOption {
        type = lib.types.str;
        description = "GitLab runner name";
      };
      options.ghc-gitlab-runner.enableLint = lib.mkOption {
        type = lib.types.bool;
        description = "Enable lint runners";
      };
      options.ghc-gitlab-runner.cores = lib.mkOption {
        type = lib.types.int;
        description = "Total core count use for GHC builds";
      };
    };

    nixosModules.ghc-runner = {config, ...}: {
      imports = [
        (import ./nixos/ci-worker.nix {
          name = config.ghc-gitlab-runner.name;
          cores = config.ghc-gitlab-runner.cores;
          enableLint = config.ghc-gitlab-runner.enableLint;
        })
      ];
    };

    nixosModules.ghc-runner-freebsd = {config, ...}: {
      imports = [
        (import ./nixos/freebsd-runner.nix {
          runnerName = config.ghc-gitlab-runner.name;
          cores = 4;
        })
      ];
    };
  };
}

