#!/usr/bin/env bash

sys=$1

if [[ $sys == "ghc-arm-5" ]]; then
    desc="ghc-arm-5"
    tags="aarch64-linux,armv7-linux,docker"
    maint="Replacement for an older ghc-arm-5 (runner #651) using the new *runner authentication tokens*"
elif [[ $sys == "ghc-ci-perf" ]]; then
    desc="ghc-ci-perf"
    tags="x86_64-linux-perf"
    maint="Replacement for an older ghc-ci-perf (runner #718) using the new *runner authentication tokens*"
fi

curl --fail-with-body --request POST --url "https://gitlab.haskell.org/api/v4/user/runners" \
  --data "runner_type=instance_type" \
  --data "description=$desc" \
  --data "tag_list=$tags" \
  --data "maintenance_note=$maint" \
  --header "PRIVATE-TOKEN: $GITLAB_WRITE" \
