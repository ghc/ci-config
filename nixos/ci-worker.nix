# GHC GitLab CI Worker
# ====================
#
# Usage:
#
# 1. Drop this file in /etc/nixos.
#
# 2. Add the following to the import list of /etc/nixos/configuration.nix:
#
#      (import ./ghc-ci-worker.nix {
#         name = ="ghc-ci-1";
#         cores = 48;
#       })
#
# 3. Drop the appropriate ghc-ci-gitlab-registration files into /root
#
# 4. Build and switch to the configuration,
#
#      nixos-rebuild switch
#
#    gitlab-runner will likely fail spuriously.
#
# 5. Restart gitlab-runner,
#
#      systemctl restart gitlab-runner

{ name, cores, enableLint ? false }:
{ pkgs, lib, ... }:

let
  gitlabUrl = "https://gitlab.haskell.org/";

  mkRunnerService = {
    name,
    configFileSuffix,
    dockerImage ? "debian/jessie",
    dockerVolumes ? [],
    limit,
    perJobCores,
    registrationFlags ? [] }:
  {
    "${name}" = {
      authenticationTokenConfigFile = "/root/ghc-ci-gitlab-registration${configFileSuffix}";
      executor = "docker";
      registrationFlags = [
        "--output-limit=16000"
      ] ++ registrationFlags;
      inherit limit dockerImage;
      environmentVariables = {
        CPUS = "${toString perJobCores}";
      };
    };
  };
in
{
  environment.systemPackages = with pkgs; [
    htop vim
  ];

  virtualisation.docker = {
    enable = true;
    autoPrune = {
      enable = true;
      flags = [ "--force" "--all" "--volumes" ];
      dates = "daily";
    };
  };

  services.gitlab-runner = {
    enable = true;
    settings.concurrent = cores / 4 + 2;
    services = (mkRunnerService {
      inherit name;
      perJobCores = 4;
      limit = cores / 4;
      configFileSuffix = "";
    }) // lib.optionalAttrs enableLint (mkRunnerService {
      name = "${name}-lint";
      perJobCores = 2;
      limit = 4;
      configFileSuffix = "-lint";
    });
  };

  users.users.gitlab-runner = {
    isNormalUser = true;
    group = "gitlab-runner";
  };
  users.groups.gitlab-runner = {};
}
