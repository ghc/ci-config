{ runnerName, cores }:

{ pkgs, lib, ... }:

let
  perJobCores = 4;

  mkRunner = { name, configDir, registrationFlags ? [], configFileSuffix }:
    let
      bin = "${../gitlab-runner-vagrant}/runner.py";
      dir = "/var/lib/gitlab-vagrant/${name}";
    in {
      systemd.tmpfiles.rules = [
        "d ${dir} 1777 gitlab-runner gitlab-runner -"
        "L ${dir}/config.json 444 gitlab-runner gitlab-runner - ${configDir}/config.json"
      ];

      services.gitlab-runner.services.${name} = {
        authenticationTokenConfigFile = "/root/ghc-ci-gitlab-registration${configFileSuffix}";
        executor = "custom";
        registrationFlags =
          let
            binArgs = step:
              let
                script = pkgs.writeScript "gitlab-runner-${name}-${step}.sh" ''
                  #!/bin/sh
                  export GITLAB_RUNNER_VAGRANT_CONFIG="${configDir}"
                  export VAGRANT_FILE="${configDir}/Vagrantfile"
                  export VAGRANT="${pkgs.vagrant}/bin/vagrant"
                  export PATH="${pkgs.polkit.bin}/bin:${pkgs.openssh}/bin:${pkgs.curl}/bin:$PATH"
                  ${pkgs.python3}/bin/python ${bin} ${step} ${dir} $@
                '';
              in ["--custom-${step}-exec=${script}"];
          in [
            "--output-limit=16000"
          ]
          ++ binArgs "config" ++ binArgs "prepare" ++ binArgs "run" ++ binArgs "cleanup"
          ++ registrationFlags;
        limit = cores / perJobCores;
        environmentVariables = {
          CPUS = "${toString perJobCores}";
        };
      };
    };
in
{
  imports = [
    (mkRunner {
      name = "${runnerName}-vagrant-freebsd13";
      configDir = ../freebsd13;
      configFileSuffix = "-freebsd13";
    })
  ];

  # We need filesystem access
  systemd.services.gitlab-runner.serviceConfig = {
    DynamicUser = lib.mkForce false;
    User = "gitlab-runner";
    Group = "gitlab-runner";
  };

  virtualisation.libvirtd.enable = true;

  users.groups.gitlab-runner = { };
  users.users.gitlab-runner = {
    createHome = true;
    description = "GitLab Runner";
    group = "gitlab-runner";
    extraGroups = [ "libvirtd" ];
    isNormalUser = true;
  };
}
