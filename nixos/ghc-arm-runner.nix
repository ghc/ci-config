{ config, ...}: {
  # Use GitLab for concurrency rather than Nix. This is an experiment.
  nix.settings = {
    cores = 1;
    max-jobs = 1;
  };

  nix.gc = {
    automatic = true;
    randomizedDelaySec = "1 hour";
    options = "--delete-older-than 30d";
  };
  system.autoUpgrade = {
    enable = true;
    allowReboot = true;
    randomizedDelaySec = "1 hour";
    # Do it in the morning to avoid running into the nightly run
    dates = "10:00";
  };

  virtualisation.docker = {
    enable = true;
    autoPrune = {
      enable = true;
      flags = [ "--force" "--all" "--volumes" ];
      dates = "daily";
    };
  };

  services.gitlab-runner = {
    enable = true;
    clear-docker-cache.enable = true;

    # Max overall concurrency for this server.
    # Currently using c3.large.arm64.
    # 250GiB RAM and ~5GiB per GHC = 50 jobs.
    # 80 cores and 5 per job = 16 jobs.
    # min(16,50) = 16.
    settings.concurrent = 16;

    services."main"= {
      executor = "docker";
      # Default image (never used since we always specify one in our jobs)
      dockerImage = "alpine";
      authenticationTokenConfigFile = "/root/runner-auth-token";

      environmentVariables = { CPUS = "5"; };

      # Escape hatch for arguments not Nixified ...
      registrationFlags = ["--output-limit 16000"];
    };
  };

  # Lots of sketchy ssh attempts come in.
  services.fail2ban.enable = true;
}
