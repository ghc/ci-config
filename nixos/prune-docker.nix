# https://gitlab.haskell.org/ghc/ci-config/-/issues/5
#
# CI images fill up over time.
#
# Long-term solution is https://github.com/stepchowfun/docuum
{ ... }:
{
  virtualisation.docker = {
    autoPrune = {
      enable = true;
      flags = [ "--force" "--all" "--volumes" ];
      dates = "daily";
    };
  };
}
