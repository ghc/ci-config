#!/usr/bin/env bash

set -Eeuo pipefail

sync () {
    file=$1
    host=$2
    scp $file root@$host:/etc/nixos
    ssh root@$host nixos-rebuild switch
}

sync ghc-arm-runner.nix ghc-arm-5

sync ghc-perf-runner.nix ghc-ci-perf-1
