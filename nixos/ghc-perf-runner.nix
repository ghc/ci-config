{ ... }: {
  # This runner only runs one job at a time, so we can build at our leisure.
  nix.settings = {
    cores = 5;
    max-jobs = 4;
  };
  nix.gc = {
    automatic = true;
    randomizedDelaySec = "1 hour";
    options = "--delete-older-than 30d";
  };
  system.autoUpgrade = {
    enable = true;
    allowReboot = true;
    randomizedDelaySec = "1 hour";
  };

  virtualisation.docker = {
    enable = true;
    autoPrune = {
      enable = true;
      flags = [ "--force" "--all" "--volumes" ];
      dates = "daily";
    };
  };

  services.gitlab-runner = {
    enable = true;
    clear-docker-cache.enable = true;

    # Since this is for performance testing, only run one job at a time.
    settings.concurrent = 1;

    services."perf" = {
      executor = "docker";
      # Default image (never used since we always specify one in our jobs)
      dockerImage = "alpine";
      authenticationTokenConfigFile = "/root/runner-auth-token";

      environmentVariables = { CPUS = "4"; };


      # Escape hatch for arguments not Nixified ...
      registrationFlags =
        [ "--output-limit 16000"
          # This runner has the capabilitiy `CAP_PERFMON`, thus it can execute
          # `perf` in its docker containers.
          # This allows us to run `perf` based tests in GHC CI.
          # See https://gitlab.haskell.org/ghc/ghc/-/merge_requests/7414
          # See https://man7.org/linux/man-pages/man7/capabilities.7.html for
          # the supported capabilities and https://docs.docker.com/engine/reference/run/#runtime-privilege-and-linux-capabilities
          # for the docker configuration.
          "--docker-cap-add PERFMON"
        ];
    };
  };

  # Lots of sketchy ssh attempts come in.
  services.fail2ban.enable = true;
}
