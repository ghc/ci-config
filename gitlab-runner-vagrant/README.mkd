# Gitlab Vagrant executor

This is a simple GitLab executor for running jobs under virtual machines
managed by Vagrant. GHC uses it for its FreeBSD and OpenBSD builds.

## Configuration

The configuration for a particular target is embodied by a few files, all
living in a single root directory:

 * `Vagrantfile`: a standard Vagrantfile defining the virtual machine
   environment the machine should run under

 * `config.json`: a JSON file containing various configuration reported to
   `gitlab-runner`. Currently this includes:
     * `environment`: a dictionary of environment variables passed to the
       guest.

 * `runner-config.sh`: A script which is only used by `register.sh` defining
   how the runner should present to GitLab.

See `../freebsd` for a concrete example of how this fits together.
