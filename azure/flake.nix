{
  description = "GHC Azure runner tools";

  inputs.nixpkgs.url = "github:nixos/nixpkgs";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShells.default = pkgs.mkShell {
          packages = [
            pkgs.azure-cli
            (pkgs.python3.withPackages (ps: [ ps.python-gitlab ]))
          ];
        };
      }
    );
}
