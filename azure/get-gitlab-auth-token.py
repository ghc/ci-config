#!/usr/bin/env python3

import argparse
import gitlab

def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument('name')
    args = parser.parse_args()

    gl = gitlab.Gitlab.from_config('haskell')
    gl.auth()
    runner = gl.user.runners.create({
        "runner_type": "instance_type",
        "description": f"Azure x86_64/linux {args.name}",
        "tag_list": ["x86_64-linux", "docker", "trusted"]
    })
    print(runner._attrs['token'])

if __name__ == '__main__':
    main()
