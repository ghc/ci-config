#!/usr/bin/env bash
set -e

N="$1"
if [ -z "$N" ]; then
    echo "usage: $0 (N)" >&2
    exit 1
fi

CORES=8
JOBS=2
CPUS_PER_JOB=$((CORES / JOBS))

name="azure-ghc-ci-linux-$N"
rgroup="ghc-ci-linux"
# Discover using `az vm image list --publisher=Canonical --offer=ubuntu-24_04-lts --architecture=x64 --all`
image="Canonical:ubuntu-24_04-lts-daily:server-gen1:24.04.202409200"
size="Standard_D${CORES}as_v4"
subscription="bf794cf8-0499-42a9-b9a8-412ed0a60a86"

if [ -z "$GITLAB_AUTH_TOKEN" ]; then
    GITLAB_AUTH_TOKEN="$(./get-gitlab-auth-token.py "$N")"
fi

# Generate cloud-init.txt
eval "cat <<EOF
$(<cloud-init.txt.tmpl)
EOF
" 2> /dev/null > cloud-init.txt

echo "Creating $name in $rgroup..."
echo ""
echo "cloud-init.txt"
echo "=============="
cat cloud-init.txt
echo "=============="

az vm create \
    --subscription="$subscription" \
    --resource-group="$rgroup" \
    --size="$size" \
    --name="$name" \
    --image="$image" \
    --os-disk-size-gb=512 \
    --admin-username="ghc-ci" \
    --authentication-type=ssh \
    --ssh-key-name="azure-ghc-ci" \
    --public-ip-sku Standard \
    --custom-data=./cloud-init.txt

rm -f cloud-init.txt

